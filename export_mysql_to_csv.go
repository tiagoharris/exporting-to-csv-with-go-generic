//  This Go script exports a given MySQL table data to a CSV file.
//
//  author: Tiago Melo (tiagoharris@gmail.com)

package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"fmt"
	"strings"
	"encoding/csv"
	"os"
	"path/filepath"
)

// helper function to handle errors
func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

// reads all records from a table 
// returns a bidimensional array with the data read
func getLinesFromTable(tableName string) [][]string {
	// this is the slice that will be appended with rows from the table
	lines := make([][]string, 0)

	// sql.Open does not return a connection. It just returns a handle to the database.
	db, err := sql.Open("mysql", "root:@/csv_example")

	// A defer statement pushes a function call onto a list.
	// The list of saved calls is executed after the surrounding function returns.
	// Defer is commonly used to simplify functions that perform various clean-up actions.
	defer db.Close()

	checkError("Error getting a handle to the database", err)
	
	// Now it's time to validate the Data Source Name (DSN) to check if the connection
	// can be correctly established.
	err = db.Ping()

	checkError("Error establishing a connection to the database", err)

	rows, err := db.Query("SELECT * FROM " + tableName)

	defer rows.Close()
	
	checkError("Error creating the query", err)

	// Get column names
	columns, err := rows.Columns()
	if err != nil {
		checkError("Error getting columns from table", err)
	}

	// Make a slice for the values
	values := make([]sql.RawBytes, len(columns))

	// rows.Scan wants '[]interface{}' as an argument, so we must copy the
	// references into such a slice
	// See http://code.google.com/p/go-wiki/wiki/InterfaceSlice for details
	scanArgs := make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}

	// now let's loop through the table lines and append them to the slice declared above
	for rows.Next() {
		// read the row on the table
		// each column value will be stored in the slice
		err = rows.Scan(scanArgs...)

		checkError("Error scanning rows from table", err)

		var value string
		var line [] string

		for _, col := range values {
			// Here we can check if the value is nil (NULL value)
			if col == nil {
				value = "NULL"
			} else {
				value = string(col)
				line = append(line, value)
			}
		}

		lines = append(lines, line)
	}

	checkError("Error scanning rows from table", rows.Err())

	return lines
}

// writes all data from a bidimensional array into a csv file
// returns the absolute path of the written file
func writeLinesFromTableName(lines * [][]string, tableName string) string {
	fileName := tableName + ".csv"

	file, err := os.Create(fileName)

	defer file.Close()
	
	checkError("Error creating the file", err)

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, value := range *lines {
		err := writer.Write(value)

		checkError("Error writing line to the file", err)
	}

	filePath, err := filepath.Abs(filepath.Dir(file.Name()))

	checkError("Error getting file path", err)

	filePath += "/" + fileName

	return filePath
}

func exportFromTable(tableName string) {
	fmt.Printf("\nExporting data from table \"%s\" ...", tableName)

	lines := getLinesFromTable(tableName)
	filePath := writeLinesFromTableName(&lines, tableName)

	fmt.Println("\nGenerated file:", filePath)
}

func main() {
	var tableName string

	fmt.Println("Enter the desired table name: ")

	fmt.Scan(&tableName)

	switch strings.ToLower(tableName) {
	case "user":
		exportFromTable(tableName)
	case "book":
		exportFromTable(tableName)
	default:
		fmt.Println("no such table:", tableName)
	}
}
